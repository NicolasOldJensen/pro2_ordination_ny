package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
    ArrayList<Dosis> doser = new ArrayList<>();

    public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
        super(startDen, slutDen, laegemiddel);
    }

    public Dosis createDosis(LocalTime tid, double antal) {
        Dosis dosis = new Dosis(tid, antal);
        doser.add(dosis);
        return dosis;
    }

    public void deleteDosis(Dosis dosis) {
        doser.remove(dosis);
    }

    public ArrayList<Dosis> getDoser() {
        return new ArrayList<>(doser);
    }

    @Override
    public double samletDosis() {
        double antal = 0;
        for (int i = 0; i < doser.size(); i++) {
            antal += doser.get(i).getAntal() * antalDage();
        }
        return antal;
    }

    @Override
    public double doegnDosis() {
        return samletDosis() / antalDage();
    }

    @Override
    public String getType() {
        return "Daglig skæv";
    }
}
