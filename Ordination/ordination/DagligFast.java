package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {
    private Dosis[] doser;
    private static int i = 0;

    public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
        super(startDen, slutDen, laegemiddel);
        doser = new Dosis[4];
        i = 0;
    }

    public Dosis[] getDoser() {
        return doser;
    }

    public Dosis createDosis(LocalTime tid, double antal) {
        Dosis dosis = new Dosis(tid, antal);
        doser[i] = dosis;
        i++;
        return dosis;
    }

    @Override
    public double samletDosis() {
        double result = 0;
        for (int i = 0; i < doser.length; i++) {
            result += doser[i].getAntal() * antalDage();
        }
        return result;
    }

    @Override
    public double doegnDosis() {
        double result = 0;
        result = samletDosis() / antalDage();
        return result;
    }

    @Override
    public String getType() {
        return "Daglig fast";
    }
}
