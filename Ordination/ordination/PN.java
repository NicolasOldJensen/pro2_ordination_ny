package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.TreeSet;

public class PN extends Ordination {

	private double antalEnheder;
	private int antalGangeGivet = 0;
	private TreeSet<LocalDate> datoer;

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
		datoer = new TreeSet<>();
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isBefore(getStartDen()) || givesDen.isAfter(getSlutDen())) {
			return false;
		} else {
			datoer.add(givesDen);
			antalGangeGivet++;
			return true;
		}
	}

	public int antalDageMellem() {
		if (datoer.size() == 0) {
			return 0;
		} else if (datoer.size() == 1) {
			return 1;
		} else {
			return (int) ChronoUnit.DAYS.between(datoer.first(), datoer.last()) + 1;
		}
	}

	@Override
	public double doegnDosis() {
		double result = 0;
		result = (getAntalGangeGivet() * getAntalEnheder()) / antalDageMellem();
		return result;
	}

	@Override
	public double samletDosis() {
		return getAntalGangeGivet() * getAntalEnheder();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

}
