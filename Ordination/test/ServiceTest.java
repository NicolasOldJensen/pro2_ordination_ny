package test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import service.Service;

public class ServiceTest {

	private Service service;
	private Patient patient;
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		patient = service.opretPatient("070985-1153", "Finn Madsen", 83.2);
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	}

	@Test
	public void testOpretPNOrdination() {
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		patient = new Patient("070985-1153", "Finn Madsen", 83.2);
		PN pn = service.opretPNOrdination(LocalDate.of(2017, 5, 2), LocalDate.of(2017, 5, 6), patient, laegemiddel, 1);
		assertEquals(true, patient.getOrdinationer().contains(pn));
		assertEquals(1, pn.getAntalEnheder(), 0.001);
	}

	@Test
	public void testOpretDagligFastOrdination() {
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		patient = new Patient("090149-2529", "Ib Hansen", 87.7);
		DagligFast df = service.opretDagligFastOrdination(LocalDate.of(2017, 5, 5), LocalDate.of(2017, 5, 6), patient,
				laegemiddel, 2, 1, 0, 1);
		assertEquals(true, patient.getOrdinationer().contains(df));
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		laegemiddel = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		patient = new Patient("090149-2529", "Ib Hansen", 87.7);

		LocalTime[] klokkeSlet = new LocalTime[4];
		klokkeSlet[0] = LocalTime.of(9, 30);
		klokkeSlet[1] = LocalTime.of(10, 30);
		klokkeSlet[2] = LocalTime.of(13, 30);
		klokkeSlet[3] = LocalTime.of(14, 30);
		double[] antalEnheder = new double[4];
		antalEnheder[0] = 2;
		antalEnheder[1] = 1;
		antalEnheder[2] = 2;
		antalEnheder[3] = 1;

		DagligSkaev ds = service.opretDagligSkaevOrdination(LocalDate.of(2017, 5, 5), LocalDate.of(2017, 5, 6), patient,
				laegemiddel, klokkeSlet, antalEnheder);

		assertEquals(true, patient.getOrdinationer().contains(ds));
		assertEquals(4, ds.getDoser().size());
	}

	@Test
	public void testOrdinationPNAnvendt() {
		PN pn = service.opretPNOrdination(LocalDate.of(2017, 5, 2), LocalDate.of(2017, 5, 6), patient, laegemiddel, 5);
		service.ordinationPNAnvendt(pn, LocalDate.of(2017, 5, 5));
	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		assertEquals(12.48, service.anbefaletDosisPrDoegn(patient, laegemiddel), 0.001);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		PN pn = service.opretPNOrdination(LocalDate.of(2017, 5, 2), LocalDate.of(2017, 5, 6), patient, laegemiddel, 1);
		assertEquals(1, service.antalOrdinationerPrVægtPrLægemiddel(70, 90, laegemiddel));
	}

	@Test
	public void testOpretPatient() {
		assertThat(service.opretPatient("0000000001", "Bent Bentsen", 95), is(instanceOf(Patient.class)));
	}

	@Test
	public void testOpretLaegemiddel() {
		assertThat(service.opretLaegemiddel("Metformin", 50, 75, 100, "mg"), is(instanceOf(Laegemiddel.class)));
	}

}
