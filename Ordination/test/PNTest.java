package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {
	private Laegemiddel laegemiddel;
	private PN pn;

	@Before
	public void setUp() throws Exception {
		laegemiddel = new Laegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");
		pn = new PN(LocalDate.of(2017, 03, 02), LocalDate.of(2017, 03, 06), laegemiddel, 5);
	}

	@Test
	public void testGivDosis() {
		assertEquals(true, pn.givDosis(LocalDate.of(2017, 03, 03)));
		assertEquals(false, pn.givDosis(LocalDate.of(2017, 04, 11)));
	}

	@Test
	public void testSamletDosis() {
		pn.givDosis(LocalDate.of(2017, 03, 02));
		assertEquals(5, pn.samletDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis() {
		pn.givDosis(LocalDate.of(2017, 03, 02));
		assertEquals(5, pn.doegnDosis(), 0.01);
	}

	@Test
	public void testPN() {
		assertNotNull(pn);
	}

	@Test
	public void testAntalDageMellem() {
		assertEquals(2, ChronoUnit.DAYS.between(LocalDate.of(2017, 03, 02), LocalDate.of(2017, 03, 03)) + 1);
	}

}
