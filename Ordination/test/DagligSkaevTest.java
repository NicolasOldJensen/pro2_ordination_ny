package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;

public class DagligSkaevTest {
	DagligSkaev dagligSkæv;
	Laegemiddel fuci;
	Laegemiddel metho;

	@Before
	public void setUp() throws Exception {
		fuci = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		metho = new Laegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");
		dagligSkæv = new DagligSkaev(LocalDate.of(2017, 3, 1), LocalDate.of(2017, 3, 5), fuci);
	}

	@Test
	public void testSamletDosis() {
		dagligSkæv = new DagligSkaev(LocalDate.of(2017, 5, 5), LocalDate.of(2017, 5, 6), fuci);
		dagligSkæv.createDosis(LocalTime.of(10, 00), 1);
		dagligSkæv.createDosis(LocalTime.of(12, 00), 2);
		dagligSkæv.createDosis(LocalTime.of(13, 00), 1);
		assertEquals(8, dagligSkæv.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		dagligSkæv = new DagligSkaev(LocalDate.of(2017, 5, 5), LocalDate.of(2017, 5, 6), fuci);
		dagligSkæv.createDosis(LocalTime.of(10, 00), 1);
		dagligSkæv.createDosis(LocalTime.of(12, 00), 2);
		dagligSkæv.createDosis(LocalTime.of(13, 00), 1);
		assertEquals(4, dagligSkæv.doegnDosis(), 0.001);
	}

	@Test
	public void testGetType() {
		assertEquals("Daglig skæv", dagligSkæv.getType());
	}

	@Test
	public void testDagligSkaev() {
		dagligSkæv = new DagligSkaev(LocalDate.of(2017, 3, 1), LocalDate.of(2017, 3, 5), fuci);
		assertNotNull(dagligSkæv);
		assertEquals(fuci, dagligSkæv.getLaegemiddel());
		assertEquals(LocalDate.of(2017, 3, 1), dagligSkæv.getStartDen());

		dagligSkæv = new DagligSkaev(LocalDate.of(2017, 4, 5), LocalDate.of(2017, 4, 8), metho);
		assertNotNull(dagligSkæv);
		assertEquals(metho, dagligSkæv.getLaegemiddel());
		assertEquals(LocalDate.of(2017, 4, 5), dagligSkæv.getStartDen());
	}

	@Test
	public void testCreateDosis() {
		Dosis dosis1 = new Dosis(LocalTime.of(12, 00), 1.5);
		assertNotNull(dosis1);
		assertEquals(LocalTime.of(12, 00), dosis1.getTid());

		Dosis dosis2 = new Dosis(LocalTime.of(15, 00), 5);
		assertNotNull(dosis2);
		assertEquals(LocalTime.of(15, 00), dosis2.getTid());
	}

	@Test
	public void testDeleteDosis() {
		Dosis dosis2 = dagligSkæv.createDosis(LocalTime.of(15, 00), 5);
		assertEquals(true, dagligSkæv.getDoser().contains(dosis2));
		dagligSkæv.deleteDosis(dosis2);
		assertEquals(false, dagligSkæv.getDoser().contains(dosis2));
	}

	@Test
	public void testGetDoser() {
		Dosis d1 = dagligSkæv.createDosis(LocalTime.of(12, 00), 1.5);
		assertNotNull(dagligSkæv.getDoser());
		assertEquals(d1, dagligSkæv.getDoser().get(0));
	}

}
