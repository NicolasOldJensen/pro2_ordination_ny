package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;

public class OrdinationTest {
    Laegemiddel laegemiddel;
    PN pn;
    
    @Before
    public void setUp() throws Exception {
        laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        pn =
            new PN(LocalDate.of(2017, 03, 11), LocalDate.of(2017, 03, 15), laegemiddel, 2);
    }

    @Test
    public void testAntalDage() {
        assertEquals(5, pn.antalDage());
    }
}
