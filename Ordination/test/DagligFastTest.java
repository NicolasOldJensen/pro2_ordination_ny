package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;
import service.Service;

public class DagligFastTest {
    Service service;
    DagligFast dagligfast;
    Laegemiddel paracetamol;
    Laegemiddel fucidin;

    @Before
    public void setUp() throws Exception {
        service = Service.getTestService();

    }

    @Test
    public void testSamletDosis() {
//        fail("Not yet implemented");
        dagligfast = new DagligFast(LocalDate.of(2017, 5, 5), LocalDate.of(2017, 5, 6), fucidin);
        dagligfast.createDosis(LocalTime.of(8, 00), 2);
        dagligfast.createDosis(LocalTime.of(15, 00), 1);
        dagligfast.createDosis(LocalTime.of(19, 00), 4);
        dagligfast.createDosis(LocalTime.of(1, 00), 1);
        assertEquals(16, dagligfast.samletDosis(), 0.001);
    }

    @Test
    public void testDoegnDosis() {
//        fail("Not yet implemented");
        dagligfast = new DagligFast(LocalDate.of(2017, 5, 5), LocalDate.of(2017, 5, 6), fucidin);
        dagligfast.createDosis(LocalTime.of(8, 00), 2);
        dagligfast.createDosis(LocalTime.of(15, 00), 1);
        dagligfast.createDosis(LocalTime.of(19, 00), 4);
        dagligfast.createDosis(LocalTime.of(01, 00), 1);
        assertEquals(8, dagligfast.doegnDosis(), 0.001);
    }

    @Test
    public void testDagligFast() {
//        fail("Not yet implemented");
        dagligfast =
            new DagligFast(LocalDate.of(2017, 3, 2), LocalDate.of(2017, 3, 6), paracetamol);
        assertNotNull(dagligfast);
        assertEquals(paracetamol, dagligfast.getLaegemiddel());
        assertEquals(LocalDate.of(2017, 3, 2), dagligfast.getStartDen());
        
    }

    @Test
    public void testCreateDosis() {
//        fail("Not yet implemented");
        Dosis dosis1 = new Dosis(LocalTime.of(8, 00), 2);
        assertNotNull(dosis1);
        assertEquals(LocalTime.of(8, 00), dosis1.getTid());

        Dosis dosis2 = new Dosis(LocalTime.of(15, 00), 1);
        assertNotNull(dosis2);
        assertEquals(LocalTime.of(15, 00), dosis2.getTid());

        Dosis dosis3 = new Dosis(LocalTime.of(20, 00), 4);
        assertNotNull(dosis3);
        assertEquals(LocalTime.of(20, 00), dosis3.getTid());

        Dosis dosis4 = new Dosis(LocalTime.of(01, 00), 1);
        assertNotNull(dosis4);
        assertEquals(LocalTime.of(01, 00), dosis4.getTid());
    }

}
